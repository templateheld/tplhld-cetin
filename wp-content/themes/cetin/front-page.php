<?php


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
        <div class="container-fluid">
            <div class="row">
                <div id="carouselExampleControls" class="offset-2 col-lg-10 carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php
                        $slider = get_field('slider');
                        $i = 1;
                        ?>
                        <?php foreach($slider as $slide): ?>
                        <div class="carousel-item <?php if( $i == 1 ) { echo 'active'; } ?>">
                            <img class="d-block w-100" src="<?php echo $slide['slide'] ?>" >
                        </div>
                        <?php
                            $i++;
                            endforeach;
                        ?>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="offset-2 col-lg-10 uberschrift">
                    <h2>Neueste</h2>
                </div>
                <div class="offset-2 col-lg-10 beitraege">
                     <?php
                        // repeater array!
                        $beitraege = get_field('beitraege');
                    ?>
                    <div class="row">
                    <?php foreach($beitraege as $beitrag): ?>
                        <div class="col-12 col-lg-4 single">
                            <img src="<?php echo $beitrag['images-content'] ?>" />
                            <h3><?php echo $beitrag['headline'] ?></h3>
                            <p><?php echo $beitrag['text'] ?></p>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="offset-2 col-lg-10 picture">
                    <div class="row">
                        <?php
                        // repeater array!
                        $box = get_field('box');
                        ?>
                        <?php foreach($box as $content): ?>
                                <div class="col-6 col-lg-6 box">
                                    <a href="<?php echo $content['url'] ?>">
                                        <img src="<?php echo $content['bild'] ?>" />
                                        <h3><?php echo $content['ueberschrift'] ?></h3>
                                    </a>
                                </div>
                            <?php
                        endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
#get_sidebar();
get_footer();