<?php
/* Template Name: Unterseite  */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

        <div class="container-fluid">
            <div class="row">
                <div class="offset-2 col-lg-10 page-picture">
                   <?php if( get_field('content-image') ): ?>
                       <img src="<?php the_field('content-image'); ?>" />
                   <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="offset-2 col-lg-10 contentuberschrift">
                    <h2><?php the_title(); ?></h2>
                </div>
            </div>

            <div class="row">
                <div class="offset-2 col-lg-10 pagecontent">
                    <?php
                        // repeater array!
                        $ucontent2 = get_field('u-content');
                        $counter = count($ucontent2);
                    ?>
                    <?php foreach($ucontent2 as $content): ?>
                    <div class="row">
                        <?php if( $counter % 2 ): ?>
                        <div class="col-12 col-lg-5 single">
                            <img src="<?php echo $content['u-bild'] ?>" />
                        </div>
                        <div class="col-12 col-lg-5 single">
                            <?php echo $content['u-text'] ?>
                        </div>
                        <?php else: ?>
                            <div class="col-12 col-lg-5 single">
                               <?php echo $content['u-text'] ?>
                            </div>
                            <div class="col-12 col-lg-5 single">
                                <img src="<?php echo $content['u-bild'] ?>" />
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php
                        $counter++;
                        endforeach;
                    ?>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="offset-2 col-lg-10 picture">
                    <div class="row">
                        <?php
                        // repeater array!
                        $box = get_field('box');
                        ?>
                        <?php foreach($box as $content): ?>
                            <div class="col-6 col-lg-6 box">
                                <a href="<?php echo $content['url'] ?>">
                                    <img src="<?php echo $content['bild'] ?>" />
                                    <h3><?php echo $content['ueberschrift'] ?></h3>
                                </a>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </div>

			<?php
			#while ( have_posts() ) : the_post();

				#get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				#if ( comments_open() || get_comments_number() ) :
					#comments_template();
				#endif;

			#endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
