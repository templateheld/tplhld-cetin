<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
        <div class="container">
            <div class="row">
                <div id="carouselExampleControls" class="offset-2 col-md-10 carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <!--<img class="imageover" src="wp-content/themes/cetin/images/LOGO.png">-->
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-2 col-md-10 uberschrift">
                    <h2>Neueste</h2>
                </div>
                <div class="offset-2 col-md-10 beitraege">
                    <div class="col-md-4 single">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/der-weite-see.jpg">
                        <h3>Der weite See</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. <a href="#">Mehr anzeigen.</a></p>
                    </div>
                    <div class="col-md-4 single">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/allein.jpg">
                        <h3>Allein.</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do <a href="#">Mehr anzeigen.</a></p>
                    </div>
                    <div class="col-md-4 single">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/steinig2.jpg">
                        <h3>Steinig.</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut. <a href="#">Mehr anzeigen.</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-2 col-md-10 picture">
                    <div class="col-md-6 box">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/literatur2.jpg">
                            <h3>Literatur</h3>
                        </a>
                    </div>
                    <div class="col-md-6 box">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/inspiration.jpg">
                            <h3>Inspiration</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
#get_sidebar();
get_footer();