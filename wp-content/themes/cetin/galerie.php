<?php
/* Template Name: Galerie  */
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <div class="container-fluid">
                <div class="row">
                    <div class="offset-2 col-lg-10 contentuberschrift">
                        <h2><?php the_title(); ?></h2>
                    </div>
                    <div class="offset-2 col-lg-10 galerie">
                        <?php
                        $galerie = get_field('galerie');
                        ?>
                        <?php foreach($galerie as $image): ?>
                            <img src="<?php echo $image['fotos'] ?>" >
                        <?php endforeach;  ?>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="offset-2 col-lg-10 picture">
                        <div class="row">
                            <?php
                            // repeater array!
                            $box = get_field('box');
                            ?>
                            <?php foreach($box as $content): ?>
                                <div class="col-6 col-lg-6 box">
                                    <a href="<?php echo $content['url'] ?>">
                                        <img src="<?php echo $content['bild'] ?>" />
                                        <h3><?php echo $content['ueberschrift'] ?></h3>
                                    </a>
                                </div>
                            <?php
                            endforeach;
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            #while ( have_posts() ) : the_post();

            #get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            #if ( comments_open() || get_comments_number() ) :
            #comments_template();
            #endif;

            #endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
