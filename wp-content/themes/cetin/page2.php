<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

        <div class="container">
            <div class="row">
                <div class="offset-2 col-md-10 page-picture">
                    <!--<img src="<?php echo get_template_directory_uri(); ?>/images/der-weite-see.jpg">-->
                </div>
                <!--<div id="carouselExampleControls" class="offset-2 col-md-10 carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/images/slider2.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/images/slider1.jpg" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>-->
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-2 col-md-10 contentuberschrift">
                    <h2>Der weite See</h2>
                </div>
                <div class="offset-2 col-md-10 pagecontent">
                    <div class="col-md-6 single">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
                    </div>
                    <div class="col-md-6 single">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/allein.jpg">
                    </div>
                </div>
                <div class="offset-2 col-md-10 pagecontent">
                    <div class="col-md-6 single">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/allein.jpg">
                    </div>
                    <div class="col-md-6 single">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="offset-2 col-md-10 picture">
                    <div class="col-md-6 box">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/literatur2.jpg">
                            <h3>Literatur</h3>
                        </a>
                    </div>
                    <div class="col-md-6 box">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/inspiration.jpg">
                            <h3>Inspiration</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>

			<?php
			#while ( have_posts() ) : the_post();

				#get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				#if ( comments_open() || get_comments_number() ) :
					#comments_template();
				#endif;

			#endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
