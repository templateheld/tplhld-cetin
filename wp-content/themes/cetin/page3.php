<?php
 /* Template Name: Unterseite */


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <?php while (have_posts()) : the_post(); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="offset-3 col-md-8 contentuberschrift" style="text-align: center">
                    <h2><?php the_title(); ?></h2>
                </div>
                <div class="offset-3 col-md-8 text" style="text-align: center">
                    <?php the_content(); ?>

                </div>
            </div>
        </div>
        <?php endwhile; wp_reset_query(); ?>
			<?php
			#while ( have_posts() ) : the_post();

				#get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				#if ( comments_open() || get_comments_number() ) :
					#comments_template();
				#endif;

			#endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
