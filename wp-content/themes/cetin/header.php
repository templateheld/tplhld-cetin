<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <title>D. Cetin</title>
    <link rel="icon"
          type="image/png"
          href="<?php echo get_template_directory_uri(); ?>/images/180625 logo1.png">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'templateheld' ); ?></a>


	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="main-navigation col-md-2 navbar fixed-top navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav flex-column">
                    <li class="nav-item">

                    <a href="<?php echo get_home_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/LOGO.png" class="img-nav">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="/startseite">Startseite</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/inspiration">Inspiration</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/fotos">Fotos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="/literatur">Literatur</a>
                    </li>
                    <li class="nav-item second">
                        <a class="nav-link" href="/impressum">Impressum</a>
                    </li>
                    <li class="nav-item second">
                        <a class="nav-link" href="/datenschutz">Datenschutz</a>
                    </li>
                </ul>
            </div>
            <?php /*
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) ); */
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
